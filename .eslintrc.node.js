const shared = require('./.eslintrc.common');

module.exports = {
  ...shared,
  env: {
    ...shared.env,
    node: true,
  },
};
