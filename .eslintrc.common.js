module.exports = {
  env: {
    es6: true,
    mocha: true
  },
  extends: 'airbnb',
  rules: {
    'import/no-extraneous-dependencies': ['error', {'devDependencies': true}],
    'no-console': ['error', { 'allow': ['info', 'warn', 'error'] }]
  },
  globals: {
    expect: true,
    fetch: true
  }
}
