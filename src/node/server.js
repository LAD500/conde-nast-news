const server = require('./httpapp');
const endpoints = require('./endpoints');

process.title = 'conde-nast-node';

endpoints(server.app);
server.start(3000);
