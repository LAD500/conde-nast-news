const express = require('express');
const { Server } = require('http');
const path = require('path');
const cors = require('cors');

const app = express();

app.use(cors());

const http = Server(app);
let server;

const start = (port, callback) => {
  const staticPath = path.resolve(__dirname, '../../../', 'dist/public/');
  app.use(express.static(staticPath));

  server = http.listen(port || 3000, () => {
    if (callback) callback();
  });
};

const stop = () => {
  if (server) {
    server.close();
  }
};

module.exports = {
  start,
  stop,
  app,
  http,
};
