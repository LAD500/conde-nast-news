const baseConfig = require('./base-config');

const initialiseConfig = (config) => {
  const newConfig = { ...config };
  const { NEWS_API_KEY } = process.env;

  if (!NEWS_API_KEY) {
    throw new Error('environment variable NEWS_API_KEY is required');
  }

  const { newsAPI } = newConfig.datasource;

  newConfig.datasource = {
    newsAPI: {
      ...newsAPI,
      API_KEY: process.env.NEWS_API_KEY,
    },
  };

  return newConfig;
};

const assembledConfig = initialiseConfig(baseConfig);

module.exports = assembledConfig;
