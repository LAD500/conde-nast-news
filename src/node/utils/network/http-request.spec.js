const { expect } = require('chai');
const nock = require('nock');
const httpRequest = require('./http-request');

describe('httpRequest', () => {
  describe('on get success ', () => {
    it('should return response data', async () => {
      nock('http://successful.endpoint')
        .get('/')
        .reply(200, { status: 'ok' });

      const response = await httpRequest('http://successful.endpoint/');

      const body = await response.json();

      expect(response.status).to.equal(200);
      expect(body).to.deep.equal({ status: 'ok' });
    });
  });

  describe('on post success ', () => {
    it('should return response data', async () => {
      nock('http://successful.endpoint')
        .post('/', { something: 'here' })
        .reply(200, { status: 'ok' });

      const options = {
        method: 'POST',
        body: JSON.stringify({ something: 'here' }),
      };

      const response = await httpRequest('http://successful.endpoint/', options);

      const body = await response.json();

      expect(response.status).to.equal(200);
      expect(body).to.deep.equal({ status: 'ok' });
    });
  });

  describe('on endpoint fail', () => {
    it('should return error', async () => {
      nock('http://successful.endpoint').get('/').replyWithError('something awful happened');

      try {
        await httpRequest('http://successful.endpoint/');
      } catch (error) {
        expect(error.message).to.equal(
          'request to http://successful.endpoint/ failed, reason: something awful happened',
        );
      }
    });
  });

  describe('on endpoint timeout', () => {
    it('should return error', async () => {
      nock('http://timeout.endpoint')
        .get('/')
        .delay(15)
        .reply(200, { something: 'here' });

      const waitTime = 10;

      const expectedMessage = `timeout http request
       - url:http://timeout.endpoint/
       - options: {}
       - waited: ${waitTime}ms`;

      try {
        await httpRequest('http://timeout.endpoint/', undefined, waitTime);
      } catch (error) {
        expect(error.message).to.equal(expectedMessage);
      }
    });
  });
});
