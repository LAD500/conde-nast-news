module.exports = (status, message, error = {}) => {
  let httpError;

  if (Number.isInteger(error.status)) {
    httpError = error;
  } else {
    httpError = new Error(message);
    httpError.status = status;
  }

  return httpError;
};
