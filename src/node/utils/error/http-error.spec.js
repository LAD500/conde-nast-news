const { expect } = require('chai');
const httpError = require('./http-error');

describe('http-error', () => {
  describe('when provided a status code and a message', () => {
    it('should produce an error that features a status and a message', () => {
      const error = httpError(404, 'file not found');

      expect(error.status).to.equal(404);
      expect(error.message).to.equal('file not found');
    });
  });

  describe('when provided a status code and a message as default and a valid http-error', () => {
    it('should produce an error with status and a message to match the provided http-error', () => {
      const errorArg = httpError(404, 'file not found');
      const error = httpError(503, 'bad gateway', errorArg);

      expect(error.status).to.equal(404);
      expect(error.message).to.equal('file not found');
    });
  });

  describe('when provided a status code and a message as default and an error with no status', () => {
    it('should produce an error with status and a message to match the provided http-error', () => {
      const errorArg = new Error('file not found');
      const error = httpError(503, 'bad gateway', errorArg);

      expect(error.status).to.equal(503);
      expect(error.message).to.equal('bad gateway');
    });
  });
});
