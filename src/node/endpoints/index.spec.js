const nock = require('nock');
const request = require('supertest');
const { expect } = require('chai');

const NEWS_API_KEY = 'XXXXXX';
process.env.NEWS_API_KEY = NEWS_API_KEY;

const server = require('../httpapp');
const endpoints = require('.');

const mockEndpointData = {
  totalResults: 2,
  articles: [
    {
      source: {
        name: 'Theguardian.com',
      },
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.theguardian.com/',
      urlToImage: 'image url',
      publishedAt: '2002-06-03',
      content: 'content',
    },
    {
      source: {
        name: 'BBC',
      },
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.bbc.co.uk/news',
      urlToImage: 'image url',
      publishedAt: '1996-06-03',
      content: 'content',
    },
  ],
};

const expectedOutput = {
  page: 1,
  pageSize: 20,
  totalResults: 2,
  totalPages: 1,
  articles: [
    {
      newsSource: 'Theguardian.com',
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.theguardian.com/',
      image: 'image url',
      published: '2002-06-03',
      content: 'content',
    },
    {
      newsSource: 'BBC',
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.bbc.co.uk/news',
      image: 'image url',
      published: '1996-06-03',
      content: 'content',
    },
  ],
};

describe('news/latest/uk', () => {
  describe('check endpoint declared', () => {
    beforeEach(() => {
      endpoints(server.app);
      server.start(3333);
    });

    afterEach(() => {
      server.stop();
    });

    it('should return 200 with results', async () => {
      nock('https://newsapi.org')
        .get('/v2/top-headlines')
        .query({
          country: 'gb',
          apiKey: NEWS_API_KEY,
          page: 1,
          pageSize: 20,
        })
        .reply(200, mockEndpointData);

      const response = await request(server.app)
        .get('/news/latest/uk');

      expect(response.statusCode).to.equal(200);
      expect(response.body).to.deep.equal(expectedOutput);
    });
  });
});
