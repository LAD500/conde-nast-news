const newsLatestUK = require('./controllers/news-latest-uk');

module.exports = (app) => {
  app.get('/news/latest/uk', newsLatestUK);
};
