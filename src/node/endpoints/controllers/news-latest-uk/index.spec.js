const nock = require('nock');
const { expect } = require('chai');
const sinon = require('sinon');

const NEWS_API_KEY = 'XXXXXX';
process.env.NEWS_API_KEY = NEWS_API_KEY;

const newsLatestUKController = require('.');

const mockEndpointData = {
  totalResults: 2,
  articles: [
    {
      source: {
        name: 'Theguardian.com',
      },
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.theguardian.com/',
      urlToImage: 'image url',
      publishedAt: '2002-06-03',
      content: 'content',
    },
    {
      source: {
        name: 'BBC',
      },
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.bbc.co.uk/news',
      urlToImage: 'image url',
      publishedAt: '1996-06-03',
      content: 'content',
    },
  ],
};

const expectedOutput = {
  page: 1,
  pageSize: 20,
  totalResults: 2,
  totalPages: 1,
  articles: [
    {
      newsSource: 'Theguardian.com',
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.theguardian.com/',
      image: 'image url',
      published: '2002-06-03',
      content: 'content',
    },
    {
      newsSource: 'BBC',
      author: 'John Doe',
      title: 'Title',
      description: 'description',
      url: 'https://www.bbc.co.uk/news',
      image: 'image url',
      published: '1996-06-03',
      content: 'content',
    },
  ],
};

describe('newsLatestUK controller', () => {
  describe('happy paths', () => {
    describe('when provide a query, with page and page size', () => {
      let req; let res; let
        json;

      beforeEach(() => {
        req = {
          query: {
            q: 'description',
            page: 2,
            pageSize: 30,
          },
        };
        json = sinon.stub();
        res = {
          status: sinon.stub().returns({ json }),
        };
      });

      afterEach(() => {
        json.reset();
        res.status.reset();
      });

      it('should return 200 with results', async () => {
        nock('https://newsapi.org')
          .get('/v2/top-headlines')
          .query({
            q: 'description',
            country: 'gb',
            apiKey: NEWS_API_KEY,
            page: 2,
            pageSize: 30,
          })
          .reply(200, {
            ...mockEndpointData,
            totalResults: 32,
          });

        await newsLatestUKController(req, res);

        expect(res.status.getCall(0).args[0]).to.equal(200);
        expect(json.getCall(0).args[0]).to.deep.equal({
          ...expectedOutput,
          page: 2,
          pageSize: 30,
          totalResults: 32,
          totalPages: 2,
        });
      });
    });
  });

  describe('unhappy paths', () => {
    let req; let res; let
      json;

    beforeEach(() => {
      req = {
        query: {
          q: 'description',
          page: 2,
          pageSize: 30,
        },
      };
      json = sinon.stub();
      res = {
        status: sinon.stub().returns({ json }),
      };
    });

    afterEach(() => {
      json.reset();
      res.status.reset();
    });

    describe('when provide an unknown query param', () => {
      it('should return 400 with error message', async () => {
        const { query } = req;

        req = {
          query: {
            ...query,
            unknown: 'variable',
          },
        };

        await newsLatestUKController(req, res);

        expect(res.status.getCall(0).args[0]).to.equal(400);
        expect(json.getCall(0).args[0]).to.deep.equal({
          error: 'Unsupported keys in request',
        });
      });
    });

    describe('if endpoint returns an unknown status', () => {
      it('should return 502 with error message', async () => {
        nock('https://newsapi.org')
          .get('/v2/top-headlines')
          .query({
            q: 'description',
            country: 'gb',
            apiKey: NEWS_API_KEY,
            page: 2,
            pageSize: 30,
          })
          .reply(400);

        await newsLatestUKController(req, res);

        expect(res.status.getCall(0).args[0]).to.equal(502);
        expect(json.getCall(0).args[0]).to.deep.equal({
          error: 'Unexpected status code from https://newsapi.org/v2/top-headlines?apiKey=XXXXXX&country=gb&page=2&pageSize=30&q=description',
        });
      });
    });

    describe('if endpoint returns an unknown status', () => {
      it('should return 502 with error message', async () => {
        nock('https://newsapi.org')
          .get('/v2/top-headlines')
          .query({
            q: 'description',
            country: 'gb',
            apiKey: NEWS_API_KEY,
            page: 2,
            pageSize: 30,
          })
          .replyWithError('Something went wrong');

        await newsLatestUKController(req, res);

        expect(res.status.getCall(0).args[0]).to.equal(502);
        expect(json.getCall(0).args[0]).to.deep.equal({
          error: 'Error from upstream endpoint https://newsapi.org/v2/top-headlines?apiKey=XXXXXX&country=gb&page=2&pageSize=30&q=description',
        });
      });
    });

    describe('if there is a generic error', () => {
      it('should return 500 with error message', async () => {
        nock('https://newsapi.org')
          .get('/v2/top-headlines')
          .query({
            country: 'gb',
            apiKey: NEWS_API_KEY,
            page: 1,
            pageSize: 20,
          })
          .reply(200, {
            totalResults: 1,
            articles: [
              {
                ...mockEndpointData.articles[0],
                source: undefined,
              },
            ],
          });

        req = {
          query: {
            page: 1,
            pageSize: 20,
          },
        };

        await newsLatestUKController(req, res);

        expect(res.status.getCall(0).args[0]).to.equal(500);
        expect(json.getCall(0).args[0]).to.deep.equal({
          error: 'Internal Server Error',
        });
      });
    });
  });
});
