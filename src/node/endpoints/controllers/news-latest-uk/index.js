const Joi = require('joi');
const queryString = require('query-string');
const config = require('../../../config');

const httpError = require('../../../utils/error/http-error');
const httpRequest = require('../../../utils/network/http-request');

const paramsSchema = Joi.object().keys({
  q: Joi.string().optional(),
  pageSize: Joi
    .number()
    .integer()
    .min(20)
    .max(100)
    .optional(),
  page: Joi.number().min(1).optional(),
}).optional();

const validateRequestParams = (req) => {
  const { error } = Joi.validate(req.query, paramsSchema);
  if (error) {
    throw httpError(400, 'Unsupported keys in request');
  }
  return req;
};

const createQueryParams = (req) => {
  const { datasource: { newsAPI: { API_KEY: apiKey } } } = config;
  const { q, page, pageSize } = req.query;

  return {
    q,
    apiKey,
    page: parseInt(page, 10) || 1,
    pageSize: parseInt(pageSize, 10) || 20,
    country: 'gb',
  };
};

const buildURL = (query) => {
  const { datasource: { newsAPI } } = config;
  const { domain, endpoints: { topHeadlines } } = newsAPI;

  const queryStr = queryString.stringify(query);

  return `${domain}${topHeadlines}?${queryStr}`;
};

const requestNewData = async (url) => {
  let body; let
    response;

  try {
    response = await httpRequest(url);
  } catch (error) {
    throw httpError(502, `Error from upstream endpoint ${url}`);
  }

  if (response.status === 200) {
    body = await response.json();
  } else {
    throw httpError(502, `Unexpected status code from ${url}`);
  }

  return body;
};

const transformArticle = (article) => {
  const {
    source: { name },
    author,
    title,
    description,
    url,
    urlToImage,
    publishedAt,
    content,
  } = article;

  return {
    newsSource: name,
    author,
    title,
    description,
    url,
    image: urlToImage,
    published: publishedAt,
    content: content || undefined,
  };
};

const transform = (body, query) => {
  const { page, pageSize } = query;
  const { totalResults, articles } = body;

  return {
    page,
    pageSize,
    totalResults,
    totalPages: Math.ceil(totalResults / pageSize),
    articles: articles.map(article => transformArticle(article)),
  };
};

module.exports = async (req, res) => {
  try {
    const query = createQueryParams(validateRequestParams(req));

    const url = buildURL(query);

    const data = await requestNewData(url);

    const transformedData = transform(data, query);

    res.status(200).json(transformedData);
  } catch (error) {
    const resError = httpError(500, 'Internal Server Error', error);

    res.status(resError.status).json({ error: resError.message });
  }
};
