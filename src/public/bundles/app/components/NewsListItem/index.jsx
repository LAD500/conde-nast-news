import React, { Component } from 'react';
import PropTypes from 'prop-types';

const formatISODate = (isoDate) => {
  const date = new Date(Date.parse(isoDate));

  const options = { year: 'numeric', month: 'numeric', day: 'numeric' };

  return date.toLocaleDateString('gb', options);
};

class NewsListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  toggleOpen = () => {
    const { open } = this.state;

    this.setState({ open: !open });
  }

  render() {
    const { open } = this.state;
    const {
      article: {
        title,
        published,
        newsSource,
        url,
        image,
        description,
      },
    } = this.props;

    return (
      <li className="news-item">
        <div className="news-item__primary-data">
          <button
            type="button"
            className="news-item__open-button"
            onClick={this.toggleOpen}
          >
            <div className="news-item__title">{title}</div>
            <div className="news-item__additional-info">
              <div className="news-item__published">{formatISODate(published)}</div>
              <div className="news-item__news-source">{`${newsSource}`}</div>
            </div>
          </button>
          <a
            href={url}
            className="news-item__link"
          >
            visit
          </a>
          {
          open
          && (
          <div className="news-item__secondary-data">
            <div className="news-item__description">{description}</div>
            {
              image
              && <img className="news-item__image" src={image} alt={title} />
            }
          </div>
          )
        }
        </div>
      </li>
    );
  }
}

NewsListItem.propTypes = {
  article: PropTypes.shape({
    newsSource: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    published: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    image: PropTypes.string,
    author: PropTypes.string,
  }).isRequired,
};

export default NewsListItem;
