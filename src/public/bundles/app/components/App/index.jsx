import React from 'react';
import NewsList from '../NewsList';

const App = () => (
  <div className="app">
    <h1 className="app__title">
      Condé Nast
      <span className="app__title__light"> news app</span>
    </h1>

    <NewsList />
  </div>
);

export default App;
