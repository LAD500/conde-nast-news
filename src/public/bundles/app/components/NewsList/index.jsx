/* eslint-disable react/no-array-index-key */

import React, { Component } from 'react';
import classNames from 'classnames';
import httpRequest from '../../utils/network/http-request';

import NewsListItem from '../NewsListItem';
import FilterPanel from '../FilterPanel';

const queryString = require('query-string');

const LoadingState = {
  IDLE: 'IDLE',
  LOADING: 'LOADING',
  FAILED: 'FAILED',
};

const endpoint = 'http://localhost:3000/news/latest/uk';

const hideNextButton = ({ page, totalPages }) => page === totalPages;

const hidePrevButton = ({ page }) => page === 1;

const pageStatus = ({ page, totalPages }) => `${page}/${totalPages}`;

class NewsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loadstate: LoadingState.IDLE,
      results: {
        page: 1,
        pageSize: 20,
        totalResults: 0,
        totalPages: 1,
        articles: [],
      },
      query: {
        q: '',
        page: 1,
      },
    };
  }

  componentDidMount() {
    this.request();
  }

  submitFilters = (filter) => {
    this.setState({
      query: {
        page: 1,
        q: filter,
      },
    }, this.request);
  }

  moveToNext = () => {
    const { query, results: { page, totalPages } } = this.state;

    if (page === totalPages) return;

    this.setState({
      query: {
        ...query,
        page: page + 1,
      },
    }, this.request);
  }

  moveToPrev = () => {
    const { query, results: { page } } = this.state;

    if (page === 1) return;

    this.setState({
      query: {
        ...query,
        page: page - 1,
      },
    }, this.request);
  }

  createUrl = () => {
    const { query } = this.state;
    const { q, page } = query;

    const keys = {
      page,
      q,
    };

    if (!q) {
      delete keys.q;
    }

    const queryStr = queryString.stringify(keys);

    return `${endpoint}?${queryStr}`;
  }

  request = () => {
    this.setState({ loadstate: LoadingState.LOADING });

    httpRequest(this.createUrl())
      .then(response => response.json())
      .then((results) => {
        this.setState({
          loadstate: LoadingState.IDLE,
          results,
        });
      })
      .catch(() => {
        this.setState({ loadstate: LoadingState.FAILED });
      });
  }

  render() {
    const { loadstate, results } = this.state;
    const { articles } = results;

    return (
      <div
        className={
          classNames(
            'news-list__container',
            { 'news-list--loading': loadstate === LoadingState.LOADING },
          )
        }
      >
        {
          <FilterPanel submitFilters={this.submitFilters} />
        }
        {
          loadstate === LoadingState.IDLE && articles.length > 0
          && (
          <>
            <div className="news-list__paging">
              <button
                type="button"
                onClick={this.moveToPrev}
                className={
                  classNames('news-list__prev-button',
                    { hide: hidePrevButton(results) })
                }
              >
                {'<'}
              </button>
              <div className="news-list__page-status">{pageStatus(results)}</div>
              <button
                type="button"
                onClick={this.moveToNext}
                className={
                  classNames('news-list__next-button',
                    { hide: hideNextButton(results) })
                }
              >
                {'>'}
              </button>
            </div>
            <ul className="news-list">
              {
                articles.map((article, i) => <NewsListItem key={i} article={article} />)
              }
            </ul>
          </>
          )
        }
        {
          loadstate === LoadingState.IDLE && articles.length === 0
          && <div className="news-list__no-results">no results</div>
        }
      </div>
    );
  }
}

export default NewsList;
