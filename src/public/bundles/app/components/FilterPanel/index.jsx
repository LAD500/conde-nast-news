import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FilterPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      input: '',
    };
  }

  onInputChange = (e) => {
    this.setState({
      input: e.target.value,
    });
  }

  submitFilters = () => {
    const { submitFilters } = this.props;
    const { input } = this.state;

    submitFilters(input);
  }

  clearFilters = () => {
    const { submitFilters } = this.props;

    submitFilters('');

    this.setState({
      input: '',
    });
  }

  render() {
    const { input } = this.state;

    return (
      <div className="filters__container">
        <input
          type="text"
          className="filter__input"
          value={input}
          onChange={this.onInputChange}
        />
        <button
          type="button"
          className="filter__button"
          onClick={this.submitFilters}
        >
          filter
        </button>
        <button
          type="button"
          className="filter__button--clear"
          onClick={this.clearFilters}
        >
          clear
        </button>
      </div>
    );
  }
}

FilterPanel.propTypes = {
  submitFilters: PropTypes.func.isRequired,
};

export default FilterPanel;
