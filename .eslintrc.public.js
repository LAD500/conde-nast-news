const shared = require('./.eslintrc.common');

module.exports = {
  ...shared,
  parser: 'babel-eslint',
  env: {
    ...shared.env,
    browser: true,
  },
};
