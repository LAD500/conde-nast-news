#Condé Nast news app

Simple news app that pulls in information from the [newsapi.org](https://newsapi.org/) API via a node proxy and displays it in a simple React app.

You will need to obtain a license key from 

https://newsapi.org/docs/get-started

## Start up

Built to run on node 10.15.3 I recomment you install node using nvm

[For instructions on using NVM and installing node versions](https://github.com/creationix/nvm)

Once you have node installed you can run the following to use the app using 

```
npm run build
NEWS_API_KEY=XX-your-key-here-XX npm start
```

This should allow you to see the production build on `http://localhost:3000`

## Entry points

#### Node server

This basically starts from 
```
src/node/server.js
```

#### React app

Starting points for the client app are 
```
src/public/index.html
src/public/bundles/main.scss
src/public/bundles/main.js
```


## Dev CLI tools

 - ```npm run lint:js:node``` - to run eslint on js node based code
 - ```npm run lint:js:public``` - to run eslint on js browser based code
 - ```npm run lint``` - runs all lint tasks
 - ```npm run test:node``` - runs all mocha/chai node based tests
 - ```npm run test:public``` - runs all mocha/chai browser based tests
 - ```npm test``` - runs all mocha/chai tests both node and browser based
 - ```npm run build``` - builds both the node and public parts of the app, placing them in the dist folder
 - ```npm run copy:node``` - builds the node server in the dist folder
 - ```npm run build:public``` - creates a production build of the browser based code
 - ```npm run build:public:dev``` - creates the development build of the browser based code
 - ```npm run start:dev``` - spins up the browser app using webpack-dev-server an attempts to proxy to the node backend.
 
## Development build

If you want to work on this using webpack-dev-servers

first copy and start the node service

```
npm run copy:node
NEWS_API_KEY=XX-your-key-here-XX npm start
```
Then in a seperate terminal window

```
npm run start:dev
```

The dev-derver version is available on `http://localhost:9000` but proxies to `http://localhost:3000` for its node services

## General approach

I've been reasonable limited on time for this and because of this backend is currently much better tested than the frontend. With more time I would happily add more tests to the frontend using a library such as Enzyme/react-testing-library.

For the server I've  stuck to the use of require over es6 imports initially, but there would be benefit in moving the code to es6 imports to allow more straightforward code sharing between the front and backend. -httpRequest is a piece of code that could be used in a universal context. And acutually it would be possilbe to break the code down a little bit more.

The backend is basically using express, the frontend I've stuck to just using react as I didn't see a reason here for why redux might be need - its a fairly simple interface.

Fundamentally the backend holds on to the API and so never shares that with the frontend. The frontend holds on to the minimum that its needs and its state is mainly used to hold the last data request and to store some basic details to create a query for the backend.

I worked in two big blocks on this - my first effort was to produce the node server, my second effort was to focus on the the frontend code. 
