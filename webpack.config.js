const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    main: [
      './src/public/bundles/main.js',
      './src/public/bundles/main.scss',
    ],
  },
  output: {
    path: path.join(__dirname, '/dist/public/'),
    filename: 'bundles/[name].bundle.js',
    publicPath: '/',
  },
  devServer: {
    contentBase: path.join(process.cwd(), '/dist/public'),
    publicPath: '/',
    port: 9000,
    proxy: {
      '/': {
        target: 'http://localhost:3000',
        secure: false,
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /assets\/font\/.*\.(eot|otf|webp|svg|ttf|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: 'assets/font/',
            outputPath: 'assets/font/',
            name: '[name].[ext]',
          },
        },
      },
      {
        test: /assets\/img\/.*\.(svg|png|jpg|jpeg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            publicPath: 'assets/img/',
            outputPath: 'assets/img/',
            name: '[name].[ext]',
          },
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer({ grid: true })],
              sourceMap: true,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  optimization: {
    minimizer: [
      new TerserPlugin(),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  plugins: [
    new HtmlWebPackPlugin({
      alwaysWriteToDisk: true,
      template: './src/public/index.html',
      filename: 'index.html',
    }),
    new HtmlWebpackHarddiskPlugin(),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([{ from: 'src/public/assets/', to: 'assets/' }]),
    new MiniCssExtractPlugin({
      filename: 'bundles/[name].bundle.css',
      chunkFilename: 'bundles/[id].bundle.css',
    }),
  ],
};
